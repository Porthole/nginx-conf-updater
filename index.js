const { Docker } = require('node-docker-api');
const { Subject, throwError, of } = require('rxjs');
const ConfigParser = require('@webantic/nginx-config-parser');
const parser = new ConfigParser();
const { fromPromise } = require('rxjs/internal-compatibility');
const { map, catchError, filter, tap, switchMap, concatAll, mergeMap, debounceTime } = require('rxjs/operators');

const config = {};
config.socketPath = process.env.SOCKET_PATH || '/var/run/docker.sock';
config.swarmName = process.env.SWARM_NAME || 'dmz';
config.serviceName = process.env.SERVICE_NAME || 'dmz_back';
config.nginxName = process.env.NGINX_SERVICE_NAME || 'dmz_gateway';
config.upstreamName = process.env.UPSTREAM_NAME || 'socket';
config.networkName = process.env.NETWORK_NAME || 'dmz_dmz';
config.servicePort = process.env.SERVICE_PORT || '3001';
config.nginxConf = process.env.NGINX_CONF || '../dmz-ops/config/proxy.conf' || '/etc/nginx/nginx.conf';
checkConfig(config);

const event$ = new Subject();
const nginxReloadSub$ = new Subject();
let nginxConf = {};
try {
  nginxConf = parser.readConfigFile(config.nginxConf, { parseIncludes: false });
  if (!nginxConf.http || !nginxConf.http[upstream(config.upstreamName)]) {
    throw 'Bad config can not find upstream ' + config.upstreamName;
  }
} catch (e) {
  console.error('Cannot correclty parse nginx conf file at ' + config.nginxConf);
  console.error(e);
  process.exit(1);
}


const promisifyStream = stream => new Promise((resolve, reject) => {
  stream.on('data', data => event$.next(data));
  stream.on('end', resolve);
  stream.on('error', reject);
});

const docker = new Docker({ socketPath: config.socketPath });

docker.events({
  since: ((new Date().getTime() / 1000) - 5).toFixed(0)
})
  .then(stream => promisifyStream(stream))
  .catch(error => console.log(error));

const parsed$ = event$.pipe(
  map(data => {
    return JSON.parse(data);
  }),
  filter(data => data.Type === 'container'),
  filter(data => data.Actor.Attributes['com.docker.stack.namespace'] === config.swarmName),
  filter(data => data.Actor.Attributes['com.docker.swarm.service.name'] === config.serviceName),
  catchError(e => {
    console.error('parse failed');
    return throwError(e);
  })
);

// clean config nginx and rewrite them
clearUpstream$(nginxConf).pipe(
  switchMap(() => fromPromise(docker.container.list())),
  // make sur nginx conf is ok on start
  map(containers => containers.filter(c => c.data.Labels['com.docker.stack.namespace'] === config.swarmName)),
  map(containers => containers.filter(c => c.data.Labels['com.docker.swarm.service.name'] === config.serviceName)),
  mergeMap(containers => containers.map(c => fromPromise(docker.container.get(c.id).status()))),
  concatAll(),
  switchMap(container => addIpToNginxConf$(container.data.NetworkSettings.Networks[config.networkName].IPAddress))
).subscribe(() => nginxReloadSub$.next());

const start$ = parsed$.pipe(
  filter(data => data.Action === 'start'),
  switchMap(event => fromPromise(docker.container.get(event.id).status())),
  tap(container => addIpToNginxConf$(container.data.NetworkSettings.Networks[config.networkName].IPAddress))
);

const stop$ = parsed$.pipe(
  filter(data => data.Action === 'stop'),
  switchMap(event => fromPromise(docker.container.get(event.id).status())),
  switchMap(container => removeIpToNginxConf$(container.data.NetworkSettings.Networks[config.networkName].IPAMConfig.IPv4Address))
);

//dont reload nginx in chain
nginxReloadSub$.pipe(
  debounceTime(3000),
  switchMap(() => reloadNginxConf$())
).subscribe(() =>
  console.log('restarted nginx'));

start$.subscribe(() => nginxReloadSub$.next());
stop$.subscribe(() => nginxReloadSub$.next());

/**
 * Add ip to nginx conf
 * @param ip
 * @returns {Observable<string>|Observable<unknown>}
 */
function addIpToNginxConf$(ip) {
  const servers = getNginxUpstreamServers();
  let alreadyHere = false;
  for (let i = 0; i < servers.length; i++) {
    const s = servers[i];
    if (s === getUrl(ip, config.servicePort)) {
      alreadyHere = true;
      break;
    }
  }
  if (!alreadyHere) {
    servers.push(getUrl(ip, config.servicePort));
    nginxConf.http[upstream(config.upstreamName)].server = servers;
    console.log(ip + ' added');
    return writeNginxConf$();
  }
  if (alreadyHere) {
    console.warn('Server already here in ' + config.upstreamName + ', cannot add ' + getUrl(ip, config.servicePort));
    return of('');
  }
}

//sometimes it's a string sometime an array... CONSISTENCY
function getNginxUpstreamServers() {
  return typeof nginxConf.http[upstream(config.upstreamName)].server === 'string' ? [nginxConf.http[upstream(config.upstreamName)].server] : nginxConf.http[upstream(config.upstreamName)].server || [];
}

function clearUpstream$(nginxConf) {
  nginxConf.http[upstream(config.upstreamName)].server = []; //already good cause we check upstream at parsing
  return writeNginxConf$();
}

function removeIpToNginxConf$(ip) {
  const servers = getNginxUpstreamServers();
  for (let i = 0; i < servers.length; i++) {
    let s = servers[i];
    console.log(s);
    if (s === getUrl(ip, config.servicePort)) {
      servers.splice(i, 1);
      nginxConf.http[upstream(config.upstreamName)].server = servers;
      console.log(ip + ' removed');
      return writeNginxConf$();
    }
  }
  console.warn('Server has not been found in ' + config.upstreamName + ', cannot remove ' + getUrl(ip, config.servicePort));
  return of('');
}

function writeNginxConf$() {
  return fromPromise(new Promise(((resolve, reject) => {
      return parser.writeConfigFile(config.nginxConf, nginxConf, true, (err, data) => {
        if (err) {
          reject(err);
        }
        resolve(data);
      });
    }))
  );
}

function getUrl(ip, port) {
  if (!port) {
    return ip;
  }
  return ip + ':' + port;
}


const execStream = stream => new Promise((resolve, reject) => {
  stream.on('data', data => console.log(data.toString()))
  stream.on('end', resolve)
  stream.on('error', reject)
});

function reloadNginxConf$() {
  return fromPromise(docker.container.list()).pipe(
    map(containers => containers.filter(c => c.data.Labels['com.docker.stack.namespace'] === config.swarmName)),
    map(containers => containers.filter(c => c.data.Labels['com.docker.swarm.service.name'] === config.nginxName)),
    mergeMap(containers => containers.map(c => fromPromise(docker.container.get(c.id).status()))),
    concatAll(),
    switchMap(container => fromPromise(container.exec.create({
      AttachStdout: true,
      AttachStderr: true,
      Cmd: ['nginx', '-s', 'reload']
    }))),
    switchMap(exec => fromPromise(exec.start({ Detach: false }))),
    map(stream => execStream(stream))
  );
}

function checkConfig(config) {
  let hasError = false;
  console.log('Using docker sock ' + config.socketPath + '. Set SOCKET_PATH to override path');
  if (!config.swarmName) {
    hasError = true;
    console.error('No swarm name provided. Have you set env SWARM_NAME ?');
  }
  if (!config.serviceName) {
    hasError = true;
    console.error('No service name provided. Have you set env SERVICE_NAME ?');
  }
  if (!config.upstreamName) {
    hasError = true;
    console.error('No upstream name provided. Have you set env UPSTREAM_NAME ?');
  }
  if (!config.networkName) {
    hasError = true;
    console.error('No network name provided. Have you set env NETWORK_NAME ?');
  }
  if (!config.servicePort) {
    console.info('No service port provided default 80. You can override at SERVICE_PORT');
  }
  if (config.nginxConf === '/etc/nginx/nginx.conf') {
    console.info('Using default nginx config file : ' + config.nginxConf);
  }

  if (hasError) {
    console.error('There is missing configuration. Exit...');
    process.exit(1);
  }
}


function upstream(name) {
  return 'upstream ' + name;
}
