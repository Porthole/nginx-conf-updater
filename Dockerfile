FROM node:12
WORKDIR /opt/app
COPY package.json .
RUN npm i
COPY index.js /opt/app

CMD ["npm", "start"]
